#pragma once
#include <string>
#include <map>
#include "JSON.h"
#include "Array.h"

//TODO: finish
//TODO: register to Json and vice-versa

namespace BrendanLib
{
	class Register
	{
	public:
		class RegisterField
		{
		protected:
			Array<uint8_t> bitMask;
			Array<uint8_t>& ownerBitField;
		public:
			RegisterField(Array<uint8_t>* owner, Array<uint8_t> bitMask);
			RegisterField& operator=(size_t setTo);
			operator size_t();
		};

		Register(size_t bitCount);
		Register(Json json);
		Register(Array<uint8_t>* useThisArray, size_t bitCount);// "useThisArray" is a pointer in order to indicate that it should have a scope >= "this"
		~Register();
		void setField(std::string name, Array<uint8_t> bitMask);
		RegisterField& operator[](std::string name);
		uint8_t& operator[](size_t byteIndex);
		operator size_t();
		Register& operator=(size_t setTo);
		operator Json();
	protected:
		class RegisterField;
			
		Array<uint8_t> bitField;
		size_t bitCount;// necessary for bitCounts not evenly divisable by 8
		std::map<std::string, RegisterField> registerFieldSet;
		friend class RegisterField;
	};
}


/*
	Register format
	{
		"SizeHint"		:
			#,
		"Fields"	: 
			[
				{
					"FieldName":
						"...",
					"Bits":
						[ #, #, ... ]
				},
				.
				.
				.
			]
	}

*/
