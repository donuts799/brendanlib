

#include <iostream>//only cpp needs this for cerr

#include "ThreadPool.h"

namespace BrendanLib
{
	void BatchThreadPool::threadLoop(int id)
	{
		try
		{
			JobChunk threadsChunk;
			while (true)
			{
				BaseJob*		threadsCurrentJob = NULL;// job may not have been set yet

				{ std::unique_lock<std::mutex> lock(poolSignalMutex);
					
					if (isFinished) { return; }// signal for destruction

					if (!currentJob && jobQueue.isEmpty())// no tasks are queued
						poolSignal.wait(lock);// wait for next task

					if (isFinished) { return; }

					if (currentJob)// if there is a job currently being processed
					{
						// if the last chunk was already allocated
						if (currentJob->allChunksAllocated())
						{
							currentJob = NULL;
							continue;
						}
						else// still tasks left
						{
							threadsCurrentJob	= currentJob;
							threadsChunk		= threadsCurrentJob->getNextChunk();
						}
					}
					else if (!jobQueue.isEmpty())// no current job, but jobs in queue
					{
						currentJob = jobQueue.dequeue();
						currentJob->reset();

						if (jobQueue.isEmpty())
							onQueueEmptied.fire(*this);

						continue;// retrieved new job, but no gaurantee it has any tasks
					}
					else // no current job, and no jobs in queue
						continue; // do not pass go, do not collect 200
				}

				// go
				if (threadsCurrentJob)
				{
					while (threadsChunk.hasNext())
					{
						try
						{
							threadsCurrentJob->runNextTask(threadsChunk);
						}
						catch(const Exception& ex)
						{
							Exception(	&ex,
										"The callback function threw an Exception", 
										"Error", 
										"Callback Error",
										__func__, __LINE__, __FILE__
										).report();
						}
						catch (const std::exception& ex)
						{
							Exception innerException(	NULL,
														ex.what(),
														"Error",
														"Unknown",
														"", 0, ""
														);

							Exception outerException(&innerException,
								"The callback function threw an Exception",
								"Error",
								"Callback Error",
								__func__, __LINE__, __FILE__
							);

							outerException.report();
						}
						catch(...)
						{
							Exception(	NULL,
										"An unknown error occured in the callback function.",
										"Error",
										"Unknown",
										__func__, 0, __FILE__,
										"Unknown"
										).report();
						}
					}

					if (threadsCurrentJob->isCompleted())// if the job has been completed
						onJobFinished.fire(*this, *threadsCurrentJob);// fire the event to notify of job completion
				}
			}
		}
		catch (const Exception& ex)
		{
			Exception(	&ex,
						"The thread [" + std::to_string(id) + "] threw an unexpected error",
						"Critical Error",
						"ThreadPool_ThreadLoopError",
						__func__, __LINE__, __FILE__,
						"Unknown"
						).report();
		}
		catch (...)
		{
			Exception(	NULL,
						"The thread [" + std::to_string(id) + "] threw an unexpected error",
						"Error",
						"ThreadPool_ThreadLoopError",
						__func__, __LINE__, __FILE__
						"Unknown"
						).report();
		}
	}

	BatchThreadPool::BatchThreadPool(EventEngine* eventEngine, uint64_t threadCount)
	{
		// use globalEventEngine, if NULL
		if (!eventEngine) { eventEngine = const_cast<EventEngine*>(&globalEventEngine); }
		onJobFinished.setEventEngine(eventEngine);
		onQueueEmptied.setEventEngine(eventEngine);
		
		threads = Array<std::thread>((ArrayIndex)threadCount);

		int id = 0;
		for(std::thread& thread : threads)
		{
			thread = std::thread(&BatchThreadPool::threadLoop, this, id++);
		}
	}

	BatchThreadPool::~BatchThreadPool()
	{
		isFinished = true;
		poolSignal.notify_all();
		for (std::thread& thread : threads)
		{
			thread.join();
		}
	}

	void BatchThreadPool::enqueueJob(BaseJob& job)
	{
		{// scope for lock
			SafeLock<> lock(queueMutex);
			jobQueue.enqueue(&job);
		}

		poolSignal.notify_all();
	}

	JobChunk::JobChunk(TaskIndex startingIndex, TaskIndex endingIndex) : 
		currentIndex(startingIndex), 
		endingIndex(endingIndex)
	{

	}

	TaskIndex JobChunk::next()
	{
		if (hasNext())
		{
			// return count before it has been incremented
			return currentIndex++;
		}
		else
			throw ThreadPool_JobChunkOverflowException(__func__, __LINE__, __FILE__);
	}

	bool JobChunk::hasNext()
	{
		return currentIndex <= endingIndex;
	}
}
