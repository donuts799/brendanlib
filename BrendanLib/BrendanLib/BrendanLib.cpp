// BrendanLib.cpp : Defines the entry point for the console application.
//


#include <string>
#include <iostream>
#include <fstream>
#include <array>
#include <windows.h>
#include <iomanip>
#include "Exception.h"
#include "EventEngine.h"
#include "Json.h"
#include "Array.h"
#include "ThreadPool.h"
#include "Register.h"

using namespace BrendanLib;

using std::string;

void TestList()
{
	try
	{
		BasicList<int> list = BasicList<int>();
		list.enqueue(2);
		list.push(3);
		list.enqueue(4);
		list.push(5);


		for (int i : list)
		{
			std::cout << i << std::endl;
		}
	}
	catch(const Exception& ex)
	{
		ex.report();
	}
}

void TestJson()
{
	try
	{
		string object = R"(["",1,"second",2])";

		Json_ToStringCharacteristics characteristics;
		characteristics.indentCharacter = '	';
		characteristics.pretify = true;
		std::cout << Json::parse(object).serialize(characteristics);
	}
	catch(const Exception& ex)
	{
		ex.report();
	}


	string one = "[9.5f]";

	try
	{
		std::cout << Json::parse(one).serialize();
	}
	catch (const Exception& ex)
	{
		ex.report();
	}

	try
	{
		std::ifstream stream("test.json");
		std::cout << stream.good() << std::endl;
		std::cout << Json::parse(stream).serialize();
	}
	catch (Exception ex)
	{
		ex.report();
	}
}

void TestArray()
{
	Array<int> myArray = Array<int>(5);
	for (auto& i : myArray)
	{
		i = 3;
		std::cout << i << std::endl;
	}

	myArray.resize(10);

	for (auto& i : myArray)
	{
		std::cout << i << std::endl;
	}

	myArray.resize(3);

	for (auto& i : myArray)
	{
		std::cout << i << std::endl;
	}

	try
	{
		myArray[2];
		myArray[3];
	}
	catch (const IndexOutOfBoundsException& ex)
	{
		ex.badIndex;
		ex.report();
	}
}


std::mutex a;

void ThreadPoolCallback(void*, double* data, TaskIndex index)
{
	a.lock();

	std::cout << data[index] << " at index " << index << std::endl;

	a.unlock();

	if (index == 8000)
		throw IndexOutOfBoundsException(index, __func__, __LINE__, __FILE__);

	Sleep(1);
}

void OnJobFinishedCallback(BatchThreadPool&, BaseJob&)
{
	std::cout << "Job has finished" << std::endl;
	throw Exception(NULL, "I don't know???", "SEVERE", "My ass", __func__, __LINE__, __FILE__);
}

void OnQueueEmptiedCallback(BatchThreadPool&)
{
	std::cout << "Queue has been emptied" << std::endl;
}

void TestThreadPool()
{
	double* threadPoolData = new double[8001];

	for (uint64_t i = 0; i < 8001; i++)
	{
		threadPoolData[i] = (double)i;
	}

	BatchThreadPool pool;

	Job<void*, double*> job(100, ThreadPoolCallback, NULL, threadPoolData, 8001);

	pool.enqueueJob(job);

	pool.onJobFinished += OnJobFinishedCallback;
	pool.onQueueEmptied += OnQueueEmptiedCallback;

	while (!job.isCompleted()) {}

	std::cout << "Job Completed" << std::endl;
	std::cout << "Sizeof(ThreadPool): " << sizeof(BatchThreadPool) << std::endl;
	std::cout << "Sizeof(Job): " << sizeof(Job<void*, double*>) << std::endl;
}

void eventCallback(int, std::string, bool)
{
	std::cout << "Callback was executed" << std::endl;
}

void TestEvent()
{
	EventEngine engine;

	Event<int, std::string, bool> event(&engine);
	event += eventCallback;

	event.fire(3, "...", true);



}

void TestRegister()
{
	Register cr3(64);

	Array<uint8_t> cr3_BaseAddressField(8);

	cr3_BaseAddressField[0] = 0b00000000;
	cr3_BaseAddressField[1] = 0b00000000;
	cr3_BaseAddressField[2] = 0b11111111;
	cr3_BaseAddressField[3] = 0b11111111;
	cr3_BaseAddressField[4] = 0b11111111;
	cr3_BaseAddressField[5] = 0b00000000;
	cr3_BaseAddressField[6] = 0b00000000;
	cr3_BaseAddressField[7] = 0b11111111;

	cr3.setField("BaseAddress", cr3_BaseAddressField);

	cr3 = 0xFFFFFFFF;

	for (int i = 0; i < 4; i++)
		std::cout << std::hex << (int)cr3[i] << std::endl;
	
	cr3[std::string("BaseAddress")] = 0b0;

	for (int i = 0; i < 4; i++)
		std::cout << std::hex << (int)cr3[i] << std::endl;

	std::cout << std::hex << (size_t)cr3[std::string("BaseAddress")] << std::endl;
	cr3[std::string("BaseAddress")] = 0x0FF;
	std::cout << std::hex << (size_t)cr3[std::string("BaseAddress")] << std::endl;
	cr3[std::string("BaseAddress")] = 0b0010101010101;
	std::cout << std::hex << (size_t)cr3[std::string("BaseAddress")] << std::endl;


}

int main()
{
	// TestList();
	// TestJson();
	// TestArray();
	// TestThreadPool();
	 TestEvent();
	// TestRegister();



    return 0;
}
