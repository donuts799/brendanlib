
#include "EventEngine.h"

namespace BrendanLib
{
	const EventEngine globalEventEngine;

	void EventEngine::EventEngineLoop()
	{
		while (true)
		{
			std::unique_lock<std::mutex> eventLock(eventMutex);
			if (killThread) { return; }		// signal to finish
			if (eventQueue.isEmpty())
			{
				eventCondition.wait(eventLock);	//wait until there is more work to do
			}
			if (killThread) { return; }		// signal to finish

			// process events in queue
			while (!eventQueue.isEmpty())	// for each event in queue
			{
				if (killThread) { return; }

				BaseEventDescriptor* fireData = NULL;

				{
					SafeLock<> queueLock(eventFireMutex);
					fireData = eventQueue.pop();
				}

				fireData->execute();
				delete fireData;
			}
		}
	}

	EventEngine::EventEngine()
	{
		engineThread = std::thread(&EventEngine::EventEngineLoop, this);
	}

	EventEngine::~EventEngine()
	{
		//stop eventEngineLoop
		killThread = true;
		eventCondition.notify_one();
		engineThread.join();

		//delete all unused descriptors
		while (!eventQueue.isEmpty())
		{
			SafeLock<> queueLock(eventFireMutex);
			BaseEventDescriptor* toDelete = eventQueue.pop();

			delete toDelete;
		}
	}
}