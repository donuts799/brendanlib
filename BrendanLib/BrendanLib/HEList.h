#pragma once
/*
	The purpose of this list is to be Highly Extendable.
	This means that this list is designed to be continually
	extended with very little overhead.
*/

template<typename storageType, size_t partitionSize = 10>
class HEList
{
public:
	HEList();
	~HEList();
};

