#pragma once

/*
	ThreadPool.h
		BrendanLib
			BaseJob
			BatchThreadPool
			Job
*/

#include <cstdint>
#include <thread>
#include <atomic>
#include <condition_variable>

#include "BasicList.h"
#include "SafeLock.h"
#include "EventEngine.h"
#include "Array.h"

/*
Contract:

There is no guarantee that jobs will be executed in the order they are queued in the pool. This is
because one job may only need one thread, and to improve efficiency, the next job that is queued
will run on the next open thread.

When the JobFinished event fires, the job that was running is guaranteed to be finished.

Jobs can only be given to one "ThreadPool", and can only be queued once. This may change, but
currently all data stored about traversing the job is held in the job, so accessing from multiple
threads is not possible. If the job is completed, it can be reused, since the job is reset before
each job start.

TODO: should there be another threadpool that takes a set of functions, and just executes them as
threads are available?
*/

namespace BrendanLib
{
	// used to index the tasks to be run
	typedef int64_t TaskIndex;

	// create ThreadPool specific Exceptions
	class ThreadPool_JobChunkOverflowException : public Exception
	{
	public:
		ThreadPool_JobChunkOverflowException(string callingFunction, size_t callingLine, string callingFile)
		{
			severity		= "Error";
			type			= "ThreadPool_JobChunkOverflowException";
			errorMessage	= "The JobChunk was asked for an index beyond its max";
			function		= callingFunction;
			line			= callingLine;
			file			= callingFile;
			fix				= "Check if the JobChunk is completed before accessing the next task";
		}
	};



	/*
		JobChunk

		Class to allow easy grouping of tasks
	*/
	class JobChunk
	{
	public:
		/*
			JobChunk(TaskIndex, TaskIndex)

			@param startingIndex:	index for this group of tasks to start at
			@param endingIndex:		index for this group of tasks to end at

			Constructor that defines the starting position, and range of this chunk
		*/
		JobChunk(TaskIndex startingIndex = 0, TaskIndex endingIndex = 0);

		/*
			next()

			Retrieves the "next" index to be processed
		*/
		TaskIndex next();

		/*
			hasNext()

			returns true if this chunk has more tasks to be processed
		*/
		bool hasNext();

	private:
		// the next index to be processed
		TaskIndex currentIndex;

		// the last index to be processed
		TaskIndex endingIndex;
	};

	/*
		BaseJob

		Abstract class that is used to create the interface for other Jobs
	*/
	class BaseJob
	{
	public:

		/*
			getNextChunk()

			Returns the next chunk to be processed
			Returns an empty chunk if there are no more chunks to be processed
		*/
		virtual JobChunk getNextChunk()				= 0;

		/*
			runNextTask(JobChunk&)

			@param chunk: the chunk of tasks to select a task from

			Run the next task from the chunk, and return whether or not there was a task to run
		*/
		virtual bool runNextTask(JobChunk& chunk)	= 0;

		/*
			allChunksAllocated()

			Return whether or not all of the chunks for this job have been allocated
		*/
		virtual bool allChunksAllocated()			= 0;

		/*
			isCompleted()

			Return whether or not all of the tasks have been executed
		*/
		virtual bool isCompleted()					= 0;

		/*
			reset

			reset the job to the beginning of the tasks
		*/
		virtual void reset()						= 0;
	};

	/*
		BatchThreadPool

		A Thread pool that executes the tasks on each thread in batches
	*/
	class BatchThreadPool
	{
	protected:
		/*
			threadLoop(int)

			@param id: the id of the thread, used for diagnostics

			This is the function that each thread will executed continuously, processing jobs
		*/
		void threadLoop(int id);

		// The array of threads that will execute the jobs
		Array<std::thread> threads;

		// The signal that is used to notify the threads that there is something to do
		std::condition_variable poolSignal;

		// The mutex used by the "poolSignal"
		std::mutex poolSignalMutex;

		// Used to indicate to the threads that they need to stop
		bool isFinished = false;

		// The queue that will store the jobs to be executed
		BasicList<BaseJob*> jobQueue;

		// The mutex that will regulate access to the queue
		std::mutex queueMutex;

		// The currentJob, stored here for fast access, so there is no need to go through the queue
		BaseJob* currentJob = NULL;

	public:
		/*
			BatchThreadPool(EventEngine*, uint64_t)

			@param eventEngine: the engine to run the events in
			@param threadCount: the number of threads to process the jobs

			Constructor that sets the eventEngine, and starts the appropriate number of threads
		*/
		BatchThreadPool(EventEngine* eventEngine = NULL,
			uint64_t threadCount = std::thread::hardware_concurrency()
		);

		/*
			~BatchThreadPool()

			Stops all of the threads, and emties queue

			TODO: should this detach threads, to stop instantly? if so I would need to deal with
			-allocating the bool that flags to stop since it would need to stay alive after the thread
			-exits
		*/
		~BatchThreadPool();

		/*
		This function adds a job to the queue to be processed when there (is a) / are thread(s)
		available
		*/
		void enqueueJob(BaseJob& job);

		// The event that fires whenever a job finishes
		Event<BatchThreadPool&, BaseJob&> onJobFinished;

		// The event that fires whenever the queue has the last item removed
		Event<BatchThreadPool&> onQueueEmptied;
	};

	/*
		Job

		The class that describes what work is, and how to execute it in a highly parellelized manner
	*/
	template<typename ContextType, typename TaskSetType>
	class Job : public BaseJob
	{
	protected:
		// Callback type dependent on type of job
		typedef void(*CallbackFunction)(ContextType context,
			TaskSetType dataSet,
			TaskIndex taskIndex);

		// How large each chunk should be that is allocated by each thread
		uint64_t chunkSize;

		// Callback for this Job to execute when it wants to complete work
		CallbackFunction callback;

		// Context for the work to be done in
		ContextType context;

		// The data to execute on
		TaskSetType dataSet;

		// The size of the data to execute on
		TaskIndex dataSetSize;

		// The index of the start of the next chunk to be allocated
		TaskIndex currentIndex = 0;

		// The number of tasks running currently. Used to keep track of when all tasks have completed
		std::atomic_int64_t tasksRunning = 0;

	public:

		/*
			getNextChunk()

			Returns the next chunk to be processed
			Returns an empty chunk if there are no more chunks to be processed
		*/
		JobChunk getNextChunk() override;

		/*
			runNextTask(JobChunk&)

			@param chunk: the chunk of tasks to select a task from

			Run the next task from the chunk, and return whether or not there was a task to run
		*/
		bool runNextTask(JobChunk& chunk) override;

		/*
			allChunksAllocated()

			Return whether or not all of the chunks for this job have been allocated
		*/
		bool allChunksAllocated() override;

		/*
			isCompleted()

			Return whether or not all of the tasks have been executed
		*/
		bool isCompleted() override;

		/*
			reset

			reset the job to the beginning of the tasks
		*/
		void reset() override;

		/*
			Job(uint64_t, CallbackFunction, 
				ContextType, TaskSetType, 
				TaskIndex)

			@param chunkSize:	the number of Tasks each thread will allocate at a time
			@param callback:	the callback that will do all of the work from each thread
			@param context:		the context that the work will be done in
			@param dataSet:		the data that will be worked on
			@param dataSetSize:	the number of pieces of data

			Constructor
		*/
		Job(uint64_t chunkSize,
			CallbackFunction callback,
			ContextType context,
			TaskSetType dataSet,
			TaskIndex dataSetSize);
	};


	//-----------code------------//



	template<typename ContextType, typename TaskSetType>
	inline JobChunk Job<ContextType, TaskSetType>::getNextChunk()
	{
		// range is (currentIndex) to (currentIndex + chunkSize - 1) inclusive
		TaskIndex beginningIndex	= currentIndex;
		TaskIndex endingIndex		= currentIndex + (chunkSize - 1);

		// if a valid chunk was created (a zero-sized chunk is invalid)
		if (currentIndex < dataSetSize)
			tasksRunning++;// allocation of tasks is considered the start of execution

		// if we overflowed
		if (endingIndex >= dataSetSize)
			endingIndex = dataSetSize - 1;

		// change currentIndex to the start of the next chunk
		currentIndex = endingIndex + 1;

		// return the chunk that represents the determined range
		return JobChunk(beginningIndex, endingIndex);
	}

	template<typename ContextType, typename TaskSetType>
	bool Job<ContextType, TaskSetType>::runNextTask(JobChunk& chunk)
	{
		if (chunk.hasNext())
		{
			TaskIndex next = chunk.next();

			if (!chunk.hasNext())// we executed the last "TaskIndex" in the chunk
				tasksRunning--;

			callback(context, dataSet, next);

			return true;
		}
		// implied else
		return false;
	}

	template<typename ContextType, typename TaskSetType>
	bool Job<ContextType, TaskSetType>::allChunksAllocated()
	{
		return currentIndex >= dataSetSize;// if the next starting index is after the end
	}

	template<typename ContextType, typename TaskSetType>
	bool Job<ContextType, TaskSetType>::isCompleted()
	{
		return (currentIndex >= dataSetSize) && (tasksRunning == 0);
	}

	template<typename ContextType, typename TaskSetType>
	void Job<ContextType, TaskSetType>::reset()
	{
		currentIndex = 0;
	}

	template<typename ContextType, typename TaskSetType>
	Job<ContextType, TaskSetType>::Job(uint64_t chunkSize,
		CallbackFunction callback, ContextType context,
		TaskSetType dataSet, TaskIndex dataSetSize)
	{
		if (chunkSize == 0) 
			throw Exception(NULL,
							"chunkSize cannot be set to zero", 
							"Error",
							"Parameter Invalid", 
							__func__, __LINE__, __FILE__,
							"Set chunkSize to a non-zero value");

		this->chunkSize		= chunkSize;
		this->callback		= callback;
		this->context		= context;
		this->dataSet		= dataSet;
		this->dataSetSize	= dataSetSize;
	}
}
