#include "stdafx.h"
#include "ToStringCharacteristics.h"


ToStringCharacteristics ToStringCharacteristics::next()
{
	ToStringCharacteristics next = *this;
	next.indentCount++;
	return next;
}
