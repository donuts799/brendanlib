#pragma once

/*
	BasicList.h
		BrendanLib
			BasicList_EmptyException
			BasicList_IteratorOverflowException
			BasicList<T>
				Node
				Iterator
	
	This file contains the BasicList class and all supporting classes. This file is used for storing data
	in an unsorted list, with stack, queue, and Iterator accessors.
*/
#include <cstdint>
#include <string>
#include "Exception.h"

namespace BrendanLib
{
	using std::string;

	typedef size_t BasicListIndex;

	//Create necessary Exceptions for BasicList specific errors
	class BasicList_EmptyException : public Exception
	{
	public:
		BasicList_EmptyException(string callingFunction, size_t callingLine, string callingFile)
		{
			severity		= "Error";
			type			= "BasicList_EmptyException";
			errorMessage	= "The list was empty when an operation tried to access its non-existant contents";
			function		= callingFunction;
			line			= callingLine;
			file			= callingFile;
			fix				= "Make sure to check if the list is filled before trying to access it.";
		}
	};
	
	class BasicList_IteratorOverflowException : public Exception
	{
	public:
		BasicList_IteratorOverflowException(string callingFunction, size_t callingLine, string callingFile)
		{
			severity		= "Error";
			type			= "BasicList_IteratorOverflowException";
			errorMessage	= "The Iterator tried to access an element beyond the end of the list";
			function		= callingFunction;
			line			= callingLine;
			file			= callingFile;
			fix				= "Make sure to check if the list is filled before trying to access it.";
		}
	};

	/*
		BasicList<T>

		The BasicList is used for storing data in an unsorted list, with stack, queue, and Iterator accessors.
	*/
	template<typename StorageType>
	class BasicList
	{
	private:
		typedef bool(*StorageTypeEqual)(const StorageType&, const StorageType&);// compare objects to determine if
																				//  -equal
		/*
			Node
			
			Supporting class to BasicList. Stores data necessary to build a linked list
		*/
		class Node
		{
		public:
			/*
				Node(StorageType)

				@param toStore: the data to store in this node

				Constructor that provides the data to store
			*/
			Node(StorageType toStore);
			
			StorageType content;	// data to store
			Node* nextPtr = NULL;	// the next node in the list
		};

		Node* head = NULL;// the first node in the linked list
		Node* tail = NULL;// the last node in the linked list
		
	public:
		/*
			Iterator

			Class to iterate through the BasicList<T>
		*/
		class Iterator
		{
		protected:
			Node* currentNode = NULL;				// current node that "this" Iterator references within the BasicList
		public:
			friend class BasicList<StorageType>;	// created as a friend of the BasicList to allow access to raw data instead of going through public functions

			/*
				Iterator(const BasicList<StorageType>&, Node* start)

				@param data:	the list to iterate through
				@param start:	the starting "Node" for this iterator to start at.

				Constructor that specifies the BasicList, and where this Iterator should start within the list
			*/
			Iterator(Node* start);

			/*
				operator++()

				Increments the Node pointer to the next Node pointer
			*/
			Iterator& operator++();

			/*
				operator*()

				Gives access to the data stored at the "Node" referenced
			*/
			StorageType& operator*() const;

			/*
				operator!=(const Iterator&)

				@param other: the other Iterator to compare to

				Returns true if "this" Iterator, and the "other" Iterator don't reference the same index, otherwise returns false.
			*/
			bool operator!=(const Iterator& other) const;
		};

		/*
			~BasicList()

			Destructor deletes all of the Nodes in the list
		*/
		~BasicList();

		/*
			enqueue(StorageType)

			@param add: content to add to the list
			
			Add the item to the end of the list
		*/
		void enqueue(StorageType add);

		/*
			dequeue()

			Remove and return the item from the beginning of the list
		*/
		StorageType dequeue();

		/*
			push(StorageType)

			@param add: content to add to the list

			Add the item to the beginning of the list
		*/
		void push(StorageType add);

		/*
			pop()

			Remove and return the item from the beginning of the list
		*/
		StorageType pop();

		/*
			remove(StorageType, StorageTypeEqual)

			@param toRemove:		the item to remove
			@param compareFunction:	the function to compare items in the list

			Removes the first item in the list that matches "toRemove" by using 
			the "compareFunction"
		*/
		bool remove(StorageType toRemove, StorageTypeEqual compareFunction);

		/*
			isEmpty

			Returns whether or not the list is empty
		*/
		bool isEmpty();

		/*
			size()

			Returns the number of elements in the list
		*/
		BasicListIndex size();


		/*
			begin()

			Returns an Iterator that starts at the beginning of the BasicList
		*/
		Iterator begin() const
		{
			return Iterator(head);
		}

		/*
			end()

			Returns an Iterator that starts just beyond the end of the BasicList
		*/
		Iterator end() const
		{
			return Iterator(NULL);
		}
	};


	//------------CODE-------------//

	template <typename StorageType>
	BasicList<StorageType>::Node::Node(StorageType toStore)
	{
		this->content = toStore;
	}

	template <typename StorageType>
	BasicList<StorageType>::~BasicList()
	{
		// delete all of the nodes in the list
		Node* temp;
		while (head)
		{
			temp = head;
			head = head->nextPtr;
			delete temp;
		}
	}

	template <typename StorageType>
	void BasicList<StorageType>::enqueue(StorageType add)// enqueue to end
	{
		if (head)// if there are any items in the list
		{
			tail->nextPtr = new Node(add);
			tail = tail->nextPtr;
		}
		else// no items yet
		{
			head = new Node(add);
			tail = head;// if one item, head and tail are the same
		}
	}

	template<typename StorageType>
	inline StorageType BasicList<StorageType>::dequeue()
	{
		// dequeue is same as pop, so to reduce errors, dequeue is written using pop

		return pop();
	}

	template<typename StorageType>
	inline void BasicList<StorageType>::push(StorageType add)
	{
		Node* newNode = new Node(add);
		newNode->nextPtr = head;
		head = newNode;

		if (!tail)// if there wasn't a tail
			tail = newNode;
	}

	template <typename StorageType>
	StorageType BasicList<StorageType>::pop()
	{
		if (head)// if there are any elements in the list
		{
			Node* popped = head;
			StorageType poppedContent = popped->content;

			if (tail == head)// there was only one item in the queue
			{
				head = NULL;
				tail = NULL;
			}
			else // multiple items in list
				head = head->nextPtr;

			delete popped;
			return poppedContent;
		}
		else
			throw BasicList_EmptyException(__func__, __LINE__, __FILE__);
	}

	template <typename StorageType>
	bool BasicList<StorageType>::remove(StorageType toRemove, StorageTypeEqual compareFunction)
	{
		Node* traverse = head;
		Node* trailing = NULL;
		while (traverse)
		{
			if (compareFunction(traverse->content, toRemove))
			{
				if (trailing)// we aren't deleting the head
					trailing->nextPtr = traverse->nextPtr;// bypass traverse
				else
					head = traverse->nextPtr;

				if (traverse == tail)// we are deleting the tail
					tail = trailing;

				delete traverse;

				return true;
			}

			// find next
			trailing = traverse;
			traverse = traverse->nextPtr;
		}

		return false;
	}

	template <typename StorageType>
	bool BasicList<StorageType>::isEmpty()
	{
		return !head;// if there is not a head, list is empty
	}

	template<typename StorageType>
	inline BasicListIndex BasicList<StorageType>::size()
	{
		BasicListIndex size = 0;
		for (auto ignore : *this)
		{
			size++;
		}
		return size;
	}

	template<typename StorageType>
	inline BasicList<StorageType>::Iterator::Iterator(Node* start)
	{
		currentNode = start;
	}

	template<typename StorageType>
	inline typename BasicList<StorageType>::Iterator& BasicList<StorageType>::Iterator::operator++()
	{
		if (currentNode == NULL)
			throw BasicList_IteratorOverflowException(__func__, __LINE__, __FILE__);
		currentNode = currentNode->nextPtr;
		return *this;
	}

	template<typename StorageType>
	inline StorageType& BasicList<StorageType>::Iterator::operator*() const
	{
		if (!currentNode)// if the currentNode isn't defined
		{
			throw BasicList_IteratorOverflowException(__func__, __LINE__, __FILE__);
		}
		return currentNode->content;
	}

	template<typename StorageType>
	inline bool BasicList<StorageType>::Iterator::operator!=(const Iterator& other) const
	{
		return currentNode != other.currentNode;
	}

}// end of BrendanLib
