
#include "Exception.h"

namespace BrendanLib
{
	// definition of static members
	std::ostream*	Exception::errorStream = &std::cerr;
	std::mutex		Exception::errorStreamMutex;
	const string defArg = "default";

	void Exception::setErrorStream(std::ostream& newStream)
	{
		if (newStream)// only set to valid stream
		{
			SafeLock<> lock(errorStreamMutex);
			errorStream = &newStream;
		}
	}

	void Exception::report() const
	{
		SafeLock<> lock(errorStreamMutex);
		*errorStream << toString() << std::endl;
	}

	Exception::Exception()
	{
	}

	Exception::~Exception()
	{
		if (source)
			delete source;// recursively deletes children "source"s
	}

	Exception::Exception(	const Exception* source,	string errorMessage,
							string severity,			string type,
							string function,			size_t line,
							string file,				string fix	)
	{
		if (source)// if other Exception exists
			this->source = new Exception(*source);

		this->severity		= severity;
		this->type			= type;
		this->errorMessage	= errorMessage;
		this->function		= function;
		this->line			= line;
		this->file			= file;
		this->fix			= fix;
	}

	Exception::Exception(const Exception& other) : Exception()
	{
		if (other.source)
			*source = *other.source;

		severity		= other.severity;
		type			= other.type;
		errorMessage	= other.errorMessage;
		file			= other.file;
		function		= other.function;
		line			= other.line;
		fix				= other.fix;
	}

	Exception::Exception(Exception&& other) : Exception()
	{
		swap(*this, other);
	}

	Exception& Exception::operator=(Exception other)
	{
		swap(*this, other);
		return *this;
	}

	void swap(Exception& first, Exception& second)
	{
		using std::swap;

		swap(first.source, second.source);
		swap(first.severity, second.severity);
		swap(first.type, second.type);
		swap(first.errorMessage, second.errorMessage);
		swap(first.file, second.file);
		swap(first.function, second.function);
		swap(first.line, second.line);
		swap(first.fix, second.fix);
	}

	Exception Exception::clone(	Exception* sourceReplace,	string errorMessageReplace,
								string severityReplace	,	string typeReplace,
								string functionReplace	,	size_t lineReplace,
								string fileReplace		,	string fixReplace) const
	{
		Exception cloned(
							sourceReplace		== nullptr	? sourceReplace			: source,
							errorMessageReplace	!= defArg	? errorMessageReplace	: errorMessage,
							severityReplace		!= defArg	? severityReplace		: severity,
							typeReplace			!= defArg	? typeReplace			: type,
							functionReplace		!= defArg	? functionReplace		: function,
							lineReplace			!= 0		? lineReplace			: line,
							fileReplace			!= defArg	? fileReplace			: file,
							fixReplace			!= defArg	? fixReplace			: fix
						);

		return cloned;
	}

	void Exception::setSource(Exception toSet)
	{
		*source = toSet;
	}

	string Exception::toString() const
	{
		//---format string, only using fields that are defined---

		string returnString = "";
		
		if (source)
			returnString += source->toString() + "\n\n";

		if (severity != "")
			returnString += severity + " ";
		
		if (type != "")
			returnString += type + " ";

		returnString += "error has occured\n";
		returnString += "in " + file;

		if (function != "" && file != "")
			returnString += "::";

		if (function != "")
			returnString += function + "()";

		if (line)
		{
			if (function != "" || file != "")
				returnString += "::";

			returnString += std::to_string(line);
		}

		returnString += "\n";

		if (errorMessage != "")
			returnString += "errorMessage: \"" + errorMessage + "\"\n";

		if (fix != "")
			returnString += "fix: \"" + fix + "\"";
		
		return returnString;
	}

	const char* Exception::what() const
	{
		//---format string, only using fields that are defined---

		string returnString = "";

		if (source)
			returnString += source->toString() + "\n\n";

		if (severity != "")
			returnString += severity + " ";

		if (type != "")
			returnString += type + " ";

		returnString += "error has occured\n";
		returnString += "in " + file;

		if (function != "" && file != "")
			returnString += "::";

		if (function != "")
			returnString += function + "()";

		if (line)
		{
			if (function != "" || file != "")
				returnString += "::";

			returnString += std::to_string(line);
		}

		returnString += "\n";

		if (errorMessage != "")
			returnString += "errorMessage: \"" + errorMessage + "\"\n";

		if (fix != "")
			returnString += "fix: \"" + fix + "\"";

		char* copy = new char[returnString.length() + 1];
		strcpy_s(copy, returnString.length() + 1, returnString.c_str());

		return copy;//TODO: redesign so no leaked memory
	}

}// end of namespace BrendanLib