#pragma once

/*
	Json.h
		BrendanLib
			class Json_ParseStringInvalidFormatException
			class Json_InvalidTypeException
			class Json_ParseStringOverrunException
			class JsonInternalValue
			class Json
			class JsonString
			class JsonNumber
			class JsonArray
			class JsonObjectPair
			class JsonObject
			class JsonBool
			class JsonNull
	
	This file contains the Json class and all supporting classes. This file is used for parsing JSON
	formatted strings into objects, and parsing objects into JSON formatted strings
*/

#include <string>	//TODO: change to unicode strings
#include <sstream>	// std::stringstream
#include <stdint.h>
#include <set>
#include <cctype>	// std::isspace()
#include "BasicList.h"
#include "Exception.h"
#include "Array.h"
#include "Json_ToStringCharacteristics.h"

//TODO: JsonObject should have std::set replaced.
//TODO: Implement iterator for json object.
//TODO: implicit conversion to and from BrendanLib::Array ??? Why do I want this ???

namespace BrendanLib
{
	using std::string;// might be changed out later

	class Exception;

	// for reporting / figuring out the specific json type
	enum class JSON_TYPE {
		NIL,
		NUMBER,
		ARRAY,
		OBJECT,
		OBJECT_PAIR,// won't actually be used, but is present for the interface
		STRING,
		BOOL
	};

	typedef ArrayIndex	JsonArrayIndex;		// used to index json arrays
	typedef size_t		JsonStringIndex;	// used to index strings used by json


	// Create necessary Exceptions for Json specific errors (defined in cpp)
	class Json_ParseStringInvalidFormatException;

	class Json_InvalidTypeException;

	class Json_ParseStringOverrunException;

	/*
		JsonInternalValue

		Abstract class used to act as the interface of each json type
	*/
	class JsonInternalValue
	{
	public:
		/*
			~JsonInternalValue()

			Virtual destructor since class will be extended
		*/
		virtual ~JsonInternalValue();

		/*
			create()

			Creates a new blank instance of "this" type
		*/
		virtual JsonInternalValue* create() const = 0;

		/*
			clone()

			Creates a new typeof("this") that is a copy of "this"
		*/
		virtual JsonInternalValue* clone() const = 0;

		/*
			serialize(ToStringCharacteristics = ToStringCharacteristics());

			@param characteristics: determines the way the return string will be formatted

			Converts "this" into a string representation using the specified
			ToStringCharacteristics, to determine how to form the string.
		*/
		virtual string serialize(Json_ToStringCharacteristics characteristics =
			Json_ToStringCharacteristics()) const = 0;

		/*
			parse(string, JsonStringIndex&);

			@param toParse:			the string to parse, will be copied since it might be mutated
			@param currentIndex:	index of where in the string the parsing currently is.

			Parses the string starting at the specified index. as the parsing progresses it
			updates the JsonStringIndex& to reflect this change. returns
		*/
		static JsonInternalValue* parse(string toParse, JsonStringIndex& currentIndex);
			
		/*
			getType();

			Returns the type of the value
		*/
		virtual JSON_TYPE getType() const = 0;

	};//end of JsonInternalValue

	/*
		Json

		Used to store, interface with, and convert to the different json values
	*/
	class Json
	{
	protected:
		/*
			testTypeAndSet(JSON_TYPE)

			@param toSet: The JSON_TYPE to compare "this" type against

			Checks type, and if different than "toSet", changes the type to "toSet"
		*/
		void testTypeAndSet(JSON_TYPE toSet);

		JsonInternalValue* valuePtr = NULL;// points to the value that "this" represents
	public:
		/*
			Json()

			defaults value to JsonNull
		*/
		Json();

		/*
			~Json()

			Deletes valuePtr if a value has been allocated
		*/
		~Json();

		/*
			Json(bool)

			@param initialValue: initial value to set the value of the JsonBool to

			Create a Json object that is a boolean
		*/
		Json(const bool initialValue);

		/*
			Json(double)

			@param initialValue: initial value to set the value of the JsonNumber to

			Create a Json object that is a number
		*/
		Json(const double initialValue);

		/*
			Json(string)

			@param initialValue: initial value to set the value of the JsonString to

			Create a Json object that is a string
		*/
		Json(const string initialValue);

		/*
			Json(Json[], JsonArrayIndex)

			@param jsonArray:	initial value to set the value of the JsonArray to (it is copied)
			@param arrayLength:	length of the "jsonArray" that is passed in

			Create a Json object that is an array
		*/
		Json(Array<Json> jsonArray);

		/*
			Json(Json&)

			@param other: Json object to have its values copied to "this"

			Create a Json object that is of the same type as "other" and with the same values
			-(copied)
		*/
		Json(const Json& other);	// Copy constructor

		/*
			Json(Json&&)

			@param other: Json object to have its values moved to "this"

			Create a Json object that is of the same type as "other" and having "other"s values
			-moved to "this" and "other" being left blank.
		*/
		Json(Json&& other);			// Move constructor

		/*
			operator=(Json)

			@param other: Json object to have its values copied to "this"

			Create a Json object that is of the same type as "other" and with the same values
			-(copied)
		*/
		Json& operator=(Json other);// Copy assignment operator

		/*
			swap(Json&, Json&)

			@param first:	the first Json object to swap
			@param second:	the second Json object to swap

			Swap the contents of the two Json objects
		*/
		friend void swap(Json& first, Json& second);// used for move and copy assignment

		/*
			operator=(const bool)

			@param toSet: boolean value to set as the value of this object

			Set the value of the internal value to a boolean with value "toSet"
		*/
		Json& operator=(const bool toSet);

		/*
			operator=(const char* const toSet)

			@param toSet: char* value to set as the value of this object

			Set the value of the internal value to a char* string with value "toSet"
		*/
		Json& operator=(const char* const toSet);

		/*
			operator=(const double)

			@param toSet: double value to set as the value of this object

			Set the value of the internal value to a double with value "toSet"
		*/
		Json& operator=(const double toSet);

		/*
			operator=(const bool)

			@param toSet: string value to set as the value of this object

			Set the value of the internal value to a string with value "toSet"
		*/
		Json& operator=(string toSet);

		/*
			getType()

			Return the JSON_TYPE of the current InternalValue stored in this object
		*/
		JSON_TYPE getType() const;

		/*
			getTypeString(JSON_TYPE)

			@param type: the type of object to convert to a string

			Return the JSON_TYPE of the type specified
		*/
		static string getTypeString(JSON_TYPE type);

		/*
			parse(string, JsonStringIndex)

			@param toParse:			the string to parse into an object
			@param currentIndex:	a reference to an index of where to parse in the string

			Parse the string, turning it into the object realization of the json formatted string.
			Start parsing the string at the "currentIndex"
		*/
		static Json parse(string toParse, JsonStringIndex& currentIndex);

		/*
			parse(string)

			@param toParse: the string to parse into an object

			Parse the string, turning it into the object realization of the json formatted string.
			Start parsing the string at the first character in the string.
		*/
		static Json parse(string toParse);

		/*
			parse(std::istream&)

			@param stream: the stream to extract the json object from

			Parse the stream, turning it into the object realization of the json formatted string.
			Start parsing the string at the first character extracted from the stream
		*/
		static Json parse(std::istream& stream);

		/*
			operator double()

			If the internal value is of type JsonDouble, return its value, else throw error
		*/
		operator double();

		/*
			operator string()

			If the internal value is of type JsonString, return its value, else throw error
		*/
		operator string();

		/*
			operator bool()

			If the internal value is of type JsonBool, return its value, else throw error
		*/
		operator bool();

		/*
			operator[](const JsonArrayIndex)

			@param index: the index to reference in the array

			If the internal value is of type JsonArray, return the value at "index", if the "index"
			-specifies a value too large for the array, this function first extends the array,
			-filling it with JsonNull objects. If the internal value is not of type JsonArray, it is
			-set to a JsonArray value, with the proper length
		*/
		Json& operator[](const JsonArrayIndex index);

		/*
			size()

			Returns the number of items stored in this value.
			Array returns length
			Object returns count
			NIL and an unassigned valuePtr returns 0
			otherwise returns 1
		*/
		JsonArrayIndex size();

		/*
			operator[](const string)

			@param member: key of the member to return

			If the internal value is of type JsonObject, return the value with the key "key", if the
			-"key" specifies a value not currently in the set, this function adds it, storing it as
			-a JsonNull object	If the internal value is not of type JsonObject, it is set to a
			-JsonObject value, with a JsonNull value added with the key "key"
		*/
		Json& operator[](const string key);

		/*
			serialize(ToStringCharacteristics)

			@param characteristics: the characteristics that determine how to format the serialized
									-output

			Serialize this object into the json formatted string
		*/
		string serialize(Json_ToStringCharacteristics characteristics = Json_ToStringCharacteristics()) const;
		
		/*
			contains(Json, bool, bool)

			@param contained:		other Json object to check if "this" contains it
			@param verifyValues:	verify the values in the Json object being compared
			@param verifyStructure	verify only the structure of the objects being compared

			Returns true if "this" contains "contained", either its set of values or raw structure.
			-If neither verifyValues, or verifyStructure is specified, this function only verifies
			-type. If verifyValues is specified, verifyStructure is ignored
		*/
		bool contains(Json& contained, bool verifyValues = false, bool verifyStructure = true);

		/*
			skipWhiteSpace(string, JsonStringIndex&)

			@param toParse:			the string to parse into an object
			@param currentIndex:	a reference to an index of where to parse in the string

			Adjust currentIndex so that it points to the next index of a non-whitespace character
		*/
		static void skipWhiteSpace(string toParse, JsonStringIndex& currentIndex);


	};// end of json class

	// the classes extending from JsonInternalValue, will not be commented, since all comments would
	// derive from JsonInternalValue, causing needless headache writing and updating, almost
	// identical comments

	class JsonString : public JsonInternalValue
	{
	public:
		JsonString(const string initialValue);
		JsonString* create() const;
		JsonString* clone() const;
		string serialize(Json_ToStringCharacteristics characteristics = Json_ToStringCharacteristics()) const;
		static JsonString parse(string& toParse, JsonStringIndex& currentIndex);
		JSON_TYPE getType() const;
		static string charToCode(const uint32_t character);
		string value = "";
	};

	class JsonNumber : public JsonInternalValue
	{
	public:
		JsonNumber(const double initialValue);
		JsonNumber* create() const;
		JsonNumber* clone() const;
		string serialize(Json_ToStringCharacteristics characteristics = Json_ToStringCharacteristics()) const;
		static JsonNumber parse(string& toParse, JsonStringIndex& currentIndex);
		JSON_TYPE getType() const;
		double value = 0;
	};

	class JsonArray : public JsonInternalValue
	{
	public:
		JsonArray(const Array<Json> jsonArray);
		~JsonArray();
		JsonArray(const JsonArray& other);
		JsonArray(JsonArray&& other);
		JsonArray& operator=(JsonArray other);
		friend void swap(JsonArray& first, JsonArray& second);
		JsonArray* create() const;
		JsonArray* clone() const;
		string serialize(Json_ToStringCharacteristics characteristics = Json_ToStringCharacteristics()) const;
		static JsonArray parse(string& toParse, JsonStringIndex& currentIndex);
		JSON_TYPE getType() const;
		Array<Json> value;
	};

	class JsonObjectPair : public JsonInternalValue//used for each name:value pair
	{
	public:
		JsonObjectPair();
		JsonObjectPair* create() const;
		JsonObjectPair* clone() const;
		string serialize(Json_ToStringCharacteristics characteristics = Json_ToStringCharacteristics()) const;
		static JsonObjectPair parse(string& toParse, JsonStringIndex& currentIndex);
		JSON_TYPE getType() const;
		JsonString name = JsonString("");
		Json value;// defaults to Null object
		class JsonCompare
		{
		public:
			bool operator()(const JsonObjectPair* left, const JsonObjectPair* right) const
			{
				return left->name.value < right->name.value;
			}
		};
	};

	class JsonObject : public JsonInternalValue
	{
	public:
		JsonObject();
		~JsonObject();
		JsonObject(const JsonObject& other);
		JsonObject(JsonObject&& other);
		JsonObject& operator=(JsonObject other);
		friend void swap(JsonObject& first, JsonObject& second);
		JsonObject* create() const;
		JsonObject* clone() const;
		string serialize(Json_ToStringCharacteristics characteristics = Json_ToStringCharacteristics()) const;
		static JsonObject parse(string& toParse, JsonStringIndex& currentIndex);
		JSON_TYPE getType() const;
		//set has to contain pointers since JsonObjectPair is abstract
		std::set<JsonObjectPair*, JsonObjectPair::JsonCompare> value;
	};

	class JsonBool : public JsonInternalValue
	{
	public:
		JsonBool(const bool initialValue);
		JsonBool* create() const;
		JsonBool* clone() const;
		string serialize(Json_ToStringCharacteristics characteristics = Json_ToStringCharacteristics()) const;
		static JsonBool parse(string& toParse, JsonStringIndex& currentIndex);
		JSON_TYPE getType() const;
		bool value = false;
	};

	class JsonNull : public JsonInternalValue
	{
	public:
		JsonNull();
		JsonNull* create() const;
		JsonNull* clone() const;
		string serialize(Json_ToStringCharacteristics characteristics = Json_ToStringCharacteristics()) const;
		static JsonNull parse(string& toParse, JsonStringIndex& currentIndex);
		JSON_TYPE getType() const;
		// void value = NULL;//NULL doesn't need a value. "value" will be infered from its type
	};

	/*
		indent(ToStringCharacteristics)

		@param characteristics: the characteristics that determine how to format the serialized
								-output

		return a string of the correct number of indentations using the specified indent character
	*/
	string indent(Json_ToStringCharacteristics characteristics = Json_ToStringCharacteristics());

}// end of namespace BrendanLib
