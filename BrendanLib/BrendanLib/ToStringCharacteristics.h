#pragma once
#include <stdint.h>

/*
ToStringCharacteristics

This class is used to keep track of indenting/formatting of
the serialized string.
*/
class ToStringCharacteristics
{
	public:
	uint32_t indentCount = 0;		// keep track of how many indent characters are needed
	bool pretify = false;			// know whether to pretify the serialization
	char indentCharacter = '\t';	// decide which character to print as the indent (space vs. tab etc.)
	/*
	next()

	@param N/A
	@gaurantee no-throw

	Returns a ToStringCharacteristics object that has the indentCount
	incremented from "this" object. Used to advance the indenting.
	*/
	ToStringCharacteristics next();
};