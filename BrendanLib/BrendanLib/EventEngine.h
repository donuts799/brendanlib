#pragma once
/*
	EventEngine.h
		BrendanLib
			BaseEventDescriptor
			BaseEvent
			EventEngine
			Event<T...>
				EventDescriptor

	This file contains a generic Event class, and an "EventEngine" class that
	is used to execute the "Event" callbacks. This class attempts to mimic the
	general syntax of C# events
*/


#include <tuple>
#include <thread>
#include <condition_variable>

#include "BasicList.h"
#include "SafeLock.h"

/*
	Purpose:

	This file contains two main classes (BaseEventDescriptor, and BaseEvent should not be used).

	Event<EventType...> - This class allows a callback (with any specified parameters), to be called
	whenever the event is triggered (fired). It is templated to allow any types to be passed to the
	callback.

	EventEngine - This class allows the "Event" class to run more efficiently. Attaching an Event/Events
	to this engine allows the callback to be non-blocking, since it runs on the engines thread. Because
	of this, the "fire()" function has very little overhead. This means you have to worry about
	multi-threaded synchronization techniques, and the callbacks are not gauranteed to be finished by the
	time "fire()" returns.

	Contract:

	Each "Event" should only be fired from a single thread, but there can be multiple events each in
	their own thread that fire into the same engine.

	Each engine only has one thread, so callbacks don't required synchronizing between callbacks
	attached through the "Event" to the same engine. They do require synchronization if they are
	attached to different engines.
*/
namespace BrendanLib
{
	class EventEngine;// forward declare, so globalEventEngine, and "BaseEvent" know about "EventEngine"

	extern const EventEngine globalEventEngine;// if sharing a common "EventEngine" is desired


	/*
		BaseEventDescriptor

		The EventDescriptor contains all of the data necessary for a "fire" call in an "Event".
		This is the Base Descriptor that allows all Descriptors to be accessed via a common
		interface
	*/
	class BaseEventDescriptor
	{
	public:
		virtual ~BaseEventDescriptor() {}// inlined in header since it is incredibly simple

		/*
			execute()

			This function allows the owner of the event to call execute on the Event, without knowing about
			the templating.
		*/
		virtual void execute() = 0;
	private:

	};

	/*
		BaseEvent

		The Event contains all of the data necessary to setup and fire Events
		This is the Base Event that allows all Events to be accessed via a common interface
	*/
	class BaseEvent
	{
	protected:
		
		// This points to the engine that the Event will use to offload its callbacks to when fired.
		EventEngine* engine = NULL;

		/*
			passThrough(BaseEventDescriptor*)

			@param data: the list of callbacks, and the data necessary to execute the callbacks
			
			This function passes the data to the EventEngine.
			This is necessary since only the base class is a friend of "EventEngine". It allows the
			sub-class to call a function that can access the "EventEngine"s private variables using the
			special templated "EventDescriptor" that this base class doesn't know about
		*/
		inline void passThrough(BaseEventDescriptor* data);
	public:

	};

	/*
		EventEngine

		The EventEngine runs all of the callbacks whenever a descriptor is added to it
	*/
	class EventEngine
	{
	public:
		/*
			EventEngine()

			Default constructor starts the thread
		*/
		EventEngine();

		/*
			~EventEngine()
			
			Destructor stops thread, and deletes leftover descriptors
		*/
		~EventEngine();
	protected:
		friend BaseEvent;// so that BaseEvent can access "this" "eventQueue"

		/*
			EventEngineLoop()

			Function that the engineThread runs to execute all waiting descriptors
		*/
		void EventEngineLoop();

		std::mutex eventFireMutex;				// to mutex the eventQueue
		std::mutex eventMutex;					// for the eventCondition
		std::condition_variable eventCondition;	// to notify the EventLoop of new "Events"
		bool killThread = false;				// used to signal that the EventLoop should stop
		std::thread engineThread;				// thread to execute the descriptors
		BasicList<BaseEventDescriptor*>eventQueue; // stores the queue of events
	};

	/*
		Event<T...>
		
		The Event contains all of the data necessary to setup and fire Events
	*/
	template <typename... EventType>
	class Event : public BaseEvent
	{
	protected:
		// type of the callback functions unique to this event
		typedef void(*CallbackFunction)(EventType...);

		/*
			CallbackCompare(CallbackFunction, CallbackFunction)

			@param first:	the first "CallbackFunction" to compare against
			@param second:	the second "CallbackFunction" to compare against

			Used to compare callback functions to be able to remove them from the callback list.
			This (the list) may be changed to use a different interface.
		*/
		static bool CallbackCompare(CallbackFunction first, CallbackFunction second);

		/*
			EventDescriptor

			The EventDescriptor contains all of the data necessary for a "fire" call in an "Event".
		*/
		class EventDescriptor : public BaseEventDescriptor
		{
		public:
			/*
				EventDescriptor(EventType...)

				Constructor that takes all of the event data that the user wants, and stores it
				in a tuple.
			*/
			EventDescriptor(size_t& descriptorCount, std::condition_variable& deleteCondition, EventType... eventData);

			/*
				~EventDescriptor()

				Destructor deletes the data stored
			*/
			~EventDescriptor();

			/*
				execute()

				This function executes all of the callback functions and provides them
				with the data it recieved from its constructor.
			*/
			void execute();

			//TODO: change this to private
			Event* owner = NULL;

			size_t& eventsActiveDescriptorCount;
			std::condition_variable& eventsDeleteCondition;


		private:
			std::tuple<EventType...>* data;
		};

		// The list of callbackFunctions this "Event" is supposed to execute once "fired"
		BasicList<CallbackFunction> callbackFunctions;

		std::mutex callbackMutex;

		//---Data for managing deleting the event---//

		//keep track of activeDescriptors
		size_t activeDescriptors = 0;

		std::condition_variable deleteCondition;

		std::mutex deleteMutex;


	public:
		/*
			Event(EventEngine*)

			@param engine: the optional engine to run the callbacks in

			Constructor optionally takes the EventEngine that will be used to process the events. If the
			EventEngine is not provided or is NULL, it will execute callback functions using a blocking call.
		*/
		Event(EventEngine* engine = NULL);

		/*
			~Event()

			Destructor does not need to do anything since "Event" only contains local variables
		*/
		~Event();

		/*
			setEventEngine(EventEngine*)

			@param engine: the engine to run callback functions in

			Changes the EventEngine to whatever is provided. If the EventEngine is not provided or is NULL,
			it will call callback functions using a blocking call. This function should not be called while
			the event is being fired, or the results are undetermined.
		*/
		void setEventEngine(EventEngine* engine = NULL);

		/*
			fire(EventType...)

			@param eventData: the data to give to all of the callbacks

			Calls all of the callback functions and provides them with the "eventData" given
		*/
		void fire(EventType... eventData);

		/*
			addCallback(CallbackFunction)

			@param callback: function to add to the callback list
			
			Add a "CallbackFunction" to the event.
		*/
		void addCallback(CallbackFunction callback);

		/*
			operator+=(CallbackFunction)

			@param callback: function to add to the callback list

			Alternate syntax for "addCallback"
		*/
		void operator+=(CallbackFunction callback);

		/*
			removeCallback(CallbackFunction)

			@param callback: function to remove from the callback list

			Remove "callback" from the list. return bool of whether remove succeeded
		*/
		bool removeCallback(CallbackFunction callback);

		/*
			operator-=(CallbackFunction)

			@param callback: function to remove from the callback list

			Alternate syntax for "removeCallback"
		*/
		bool operator-=(CallbackFunction callback);
	};


	//---------------code---------------//

	void BaseEvent::passThrough(BaseEventDescriptor* data)
	{
		{// add the Descriptor to the queue in a thread-safe manner
			SafeLock<>(engine->eventFireMutex);
			engine->eventQueue.enqueue(data);
		}

		engine->eventCondition.notify_one();// only one engine to notify
	}

	template <typename... EventType>
	bool Event<EventType...>::CallbackCompare(CallbackFunction first, CallbackFunction second)
	{
		return first == second;
	}

	template<typename ...EventType>
	inline Event<EventType...>::EventDescriptor::EventDescriptor(size_t& descriptorCount, std::condition_variable& deleteCondition, EventType... eventData) : 
		eventsActiveDescriptorCount(descriptorCount), 
		eventsDeleteCondition(deleteCondition)
	{
		descriptorCount++;//keep track of active descriptors
		data = new std::tuple<EventType...>(eventData...);
	}

	template<typename ...EventType>
	inline Event<EventType...>::EventDescriptor::~EventDescriptor()
	{
		eventsActiveDescriptorCount--;
		if (eventsActiveDescriptorCount == 0)
		{
			eventsDeleteCondition.notify_one();// notify event that there are no active descriptors
		}
		delete data;
	}

	template <typename... EventType>
	void Event<EventType...>::EventDescriptor::execute()
	{
		SafeLock<>(owner->callbackMutex);

		for (CallbackFunction callback : owner->callbackFunctions)// for every callback
		{
			try
			{
				callback(std::get<EventType>(*data)...);// execute the callback
			}
			catch(const Exception& ex)
			{
				Exception(	&ex,
							"The callback function threw an Exception", 
							"Error", 
							"Callback Error",
							__func__, __LINE__, __FILE__
							).report();
			}
			catch (const std::exception& ex)
			{
				Exception innerException(	NULL,
											ex.what(),
											"Error",
											"Unknown",
											"", 0, ""
											);

				Exception outerException(&innerException,
					"The callback function threw an Exception",
					"Error",
					"Callback Error",
					__func__, __LINE__, __FILE__
				);

				outerException.report();
			}
			catch(...)
			{
				Exception(	NULL,
							"An unknown error occured in the callback function.",
							"Error",
							"Unknown",
							__func__, 0, __FILE__,
							"Unknown"
							).report();
			}
		}
	}

	template<typename... EventType>
	Event<EventType...>::Event(EventEngine* engine)
	{
		BaseEvent::engine = engine;
	}

	template<typename... EventType>
	Event<EventType...>::~Event()
	{
		while (activeDescriptors != 0)
		{
			std::unique_lock<std::mutex> deleteLock(deleteMutex);
			deleteCondition.wait(deleteLock);
		}
	}

	template<typename... EventType>
	void Event<EventType...>::setEventEngine(EventEngine* toSet)
	{
		BaseEvent::engine = toSet;
	}

	template<typename... EventType>
	void Event<EventType...>::fire(EventType... eventData)
	{
		// create new descriptor that captures the "fire" data
		EventDescriptor* descriptor = new EventDescriptor(activeDescriptors, deleteCondition, eventData...);
		descriptor->owner = this;

		if (engine)	// if we are supposed to use a separate thread
			passThrough(descriptor);// passthrough deletes the descriptor, so we don't have to
		else		// use a blocking call on this thread
		{
			descriptor->execute();
			delete descriptor;
		}
	}

	template<typename... EventType>
	void Event<EventType...>::addCallback(CallbackFunction callback)
	{
		SafeLock<> lock(callbackMutex);
		callbackFunctions.enqueue(callback);
	}

	template<typename... EventType>
	void Event<EventType...>::operator+=(CallbackFunction callback)
	{
		SafeLock<> lock(callbackMutex);
		callbackFunctions.enqueue(callback);
	}

	template<typename... EventType>
	bool Event<EventType...>::removeCallback(CallbackFunction callback)
	{
		SafeLock<> lock(callbackMutex);
		return callbackFunctions.remove(callback, CallbackCompare);
	}

	template<typename... EventType>
	bool Event<EventType...>::operator-=(CallbackFunction callback)
	{
		SafeLock<> lock(callbackMutex);
		return callbackFunctions.remove(callback, CallbackCompare);
	}
}
