#pragma once

/*
	Exception.h
		namespace BrendanLib
			class Exception
			class IndexOutOfBoundsException
*/
#include <iostream>
#include <string>
#include <mutex>

//#include "json.h"
#include "SafeLock.h"

namespace BrendanLib
{
	using std::string;//TODO: remove?
	
	class Json;// declare Json since json.h and this file include each other

	const extern string defArg;//default argument for clone function

	/*
		Notes:

		since setErrorStream only works for the global stream, if
		reporting to a unique ostream is desired, it must be done
		by executing the toString method, and outputting that string
		(this is exactly what is done with the report() function).

		To extend from this class, ~Exception, Exception(const Exception& other),
		need to be overridden. report(), toString(), and toJson() can be
		overridden if extra data needs reported beyond the default, or if a
		different output format is desired
	*/

	/*
		Exception

		This class is used to create a standard way of implementing exceptions.
		All Exceptions should extend from this class.
	*/
	class Exception : public std::exception
	{
	protected:
		static std::ostream*	errorStream;				// stream to output the Exceptions
		static std::mutex		errorStreamMutex;			// to syncronize the Exception outputs
		Exception*				source			= nullptr;	// the "source" is the error that caused "this" error

	public:
		/*
			Exception()

			Default constructor sets all parameters to empty/0
		*/
		Exception();

		/*
			~Exception()

			Destructor is virtual since this class is intended to be extended from.
			This destructor deletes the source if it is provided.
		*/
		virtual ~Exception();

		/*
			Exception(	Exception*,	string,
						string,		string,
						string,		uint32_t,
						string,		string)

			@param source:			the source of "this" error
			@param errorMessage:	the error message as a string
			@param severity:		the severity of the error
			@param type:			the type of error that occured
			@param function:		the function the error was detected in
			@param line:			the line the error was detected in
			@param file:			the file the error was detected in
			@param fix:				a possible fix to the error:

			Constructor that provides all information which exists in the base class
		*/
		Exception(	const Exception* source,		string errorMessage,
					string severity = "",			string type		= "",
					string function = "",			size_t line	= 0,
					string file		= "",			string fix		= ""
		);

		/*
			Exception(const Exeption&)

			@param other: the "Exception" to copy from

			Copy constructor
		*/
		Exception(const Exception& other);

		/*
			Exception(Exception&&)
			
			@param other: the "Exception to move from

			Move constructor
		*/
		Exception(Exception&& other);

		/*
			operator=(Exception)

			@param other: the "Exception to copy from

			Copy-assignment operator
		*/
		virtual Exception& operator=(Exception other);

		/*
			swap(Exception&, Exception&)

			@param first: the first "Exception" to swap
			@param second: the second "Exception" to swap

			Swap function for copy-swap idiom
		*/
		friend void swap(Exception& first, Exception& second);

		/*
			clone(	Exception*, string,
					string,		string,
					string,		uint32_t,
					string,		string)

			@param sourceReplace:		source to replace
			@param errorMessageReplace:	replacement error message
			@param severityReplace:		replacement severity
			@param typeReplace:			replacement type name
			@param functionReplace:		replacement function name
			@param lineReplace:			replacement line number
			@param fileReplace:			replacement file name
			@param fixReplace:			replacement fix instructions

			This function copies "this" Exception, but for any fields that aren't default (defArg || NULL)
			it will replace those fields with the specified values.
		*/
		Exception clone(	Exception* sourceReplace,			string errorMessageReplace	= defArg,
							string severityReplace	= defArg,	string typeReplace			= defArg,
							string functionReplace	= defArg,	size_t lineReplace			= 0,
							string fileReplace		= defArg,	string fixReplace			= defArg) const;

		/*
			setSource(Exception)

			@param source: the source of "this" Exception

			Set the source of "this" Exception
		*/
		void setSource(Exception source);

		/*
			setErrorStream(std::ostream&)

			@param newStream: the new stream to use for the global ostream

			Set the global stream to be used for reporting exceptions
		*/
		static void setErrorStream(std::ostream& newStream);

		/*
			report()

			Report the error to the global ostream, formatting the output using "this" toString method
		*/
		virtual void report() const;

		/*
			toString()

			Convert the Exception data to a well-formatted string
		*/
		virtual string toString() const;

		/*
			what()

			Convert the Exception data to a char* to cover the std::exception virtual function
		*/
		const char* what() const override;



									// ---example---
		string severity		= "";	// "CRITICAL"
		string type			= "";	// "NULL pointer"
		string errorMessage = "";	// "Could not process blah, because of NULL pointer exception"
		string function		= "";	// "foo"
		size_t line			= 0;	// 45
		string file			= "";	// "blah.cpp"
		string fix			= "";	// "make sure b is in a"

	};// end of class Exception


	// Basic Exceptions that many classes will need

	/*
		IndexOutOfBoundsException

		Exception to be thrown when an operation using an index is given an index
		that is not within the range of acceptable indexes
	*/
	class IndexOutOfBoundsException : public Exception
	{
	public:
		const size_t badIndex;// Index that was attempted to be accessed

		/*
			IndexOutOfBoundsException(	size_t, string,
										size_t, string )

			Constructor that provides information necessary to report on an IndexOutOfBoundsException
		*/
		IndexOutOfBoundsException(size_t index, string callingFunction, size_t callingLine, string callingFile) : badIndex(index)
		{
			severity		= "Error";
			type			= "IndexOutOfBounds";
			errorMessage	= "Index [" + std::to_string(index) + "] specified was not in the range of valid indexes";
			function		= callingFunction;
			line			= callingLine;
			file			= callingFile;
			fix				= "Try verifying your indexes before using them";
		}
	};

	class UnknownException : public Exception
	{
	public:
		UnknownException(string callingFunction, size_t callingLine, string callingFile)
		{
			severity		= "Error";
			type			= "UnknownException";
			errorMessage	= "Uknown Error";
			function		= callingFunction;
			line			= callingLine;
			file			= callingFile;
			fix				= "Unknown";
		}
	};

	class FileOpenException : public Exception
	{
	public:
		FileOpenException(string fileName, string callingFunction, size_t callingLine, string callingFile)
		{
			severity		= "Error";
			type			= "FileOpenException";
			errorMessage	= "Could not open file \"" + fileName + "\"";
			function		= callingFunction;
			line			= callingLine;
			file			= callingFile;
			fix				= "Please check the filename";
		}
	};

}// end of namespace BrendanLib