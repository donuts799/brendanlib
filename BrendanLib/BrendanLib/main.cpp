// Error.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Exception.h"
#include "JSON.h"
#include <iostream>

using namespace BrendanLib;

int main()
{
	Exception b(NULL, "Error Message", "CRITICAL");
	Exception a(&b, "Error Message", "CRITICAL", "Null pointer", "main", 13, "main.cpp", "not quite sure");

	a.report();

    return 0;
}

