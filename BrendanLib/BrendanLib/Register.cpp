
#include "Register.h"

namespace BrendanLib
{
	Register::Register(size_t bitCount)
	{
		ArrayIndex bytesNeeded = (bitCount + 7) / 8;// round up the byte
		bitField = Array<uint8_t>(bytesNeeded);
		this->bitCount = bitCount;
	}

	Register::Register(Json json)
	{
		if (json.getType() == JSON_TYPE::ARRAY)
		{
			//JsonArrayIndex arrayLength = json.size();

		}
		else
		{
			throw "ERROR!!!!";
		}
	}

	Register::Register(Array<uint8_t>* useThisArray, size_t bitCount)
	{
		bitField = Array<uint8_t>(useThisArray->rawArray, useThisArray->size(), false, false);
		this->bitCount = bitCount;
	}

	Register::~Register()
	{
	}

	void Register::setField(std::string name, Array<uint8_t> bitMask)
	{
		if (bitMask.size() < bitField.size())
			throw "ERROR!!!!";

		registerFieldSet.insert(std::pair<std::string, RegisterField>(name, RegisterField(&bitField, bitMask)));
	}
	
	Register::RegisterField& Register::operator[](std::string name)
	{
		return registerFieldSet.at(name);
	}

	uint8_t& Register::operator[](size_t byteIndex)
	{
		return bitField[byteIndex];
	}

	Register::operator size_t()
	{
		if (bitCount > (sizeof(size_t) * 8))
		{
			throw Exception(NULL, "Register is too large to be converted to a size_t");
		}

		size_t returnValue = 0;

		for (int i = 0; i < sizeof(size_t); i++)
		{
			returnValue |= (bitField[i]) << (i * 8);
		}

		return returnValue;
	}

	Register& Register::operator=(size_t setTo)
	{
		for (int i = (sizeof(setTo) * 8) - 1; i >= 0; i--)
		{
			if (setTo & ((size_t)1 << i))
			{
				// highest bit with a 1 in it
				if ((size_t)(i + 1) > bitCount)
				{
					throw Exception(NULL, "To large for register size");//TODO:
				}
				break;// only need to test largest
			}
		}

		size_t byteMask = 0b11111111;// to select one byte at a time

		for (int i = 0; i < bitField.size() && i < sizeof(setTo); i++, byteMask = byteMask << 8)
		{
			// set the next byte to (the next byte of the input) shifted to the lowest byte
			bitField[i] = (uint8_t) (setTo >> (i * 8));
		}

		return *this;
	}

	Register::operator Json()
	{
		// TODO:
		return Json();
	}

	Register::RegisterField::RegisterField(Array<uint8_t>* owner, Array<uint8_t> bitMask) : 
		ownerBitField(*owner)
	{
		this->bitMask		= bitMask;
	}

	Register::RegisterField& Register::RegisterField::operator=(size_t setTo)
	{
		for (size_t i = 0; i < bitMask.size(); i++)// for every byte in the register
		{
			ownerBitField[i] &= ~bitMask[i];//clear every bit for this field
		}

		for (size_t registerBit = 0, valueBit = 0; registerBit < (bitMask.size() * 8) && valueBit < sizeof(setTo) * 8 ; registerBit++)// for every bit in the register
		{
			if (bitMask[registerBit / 8] & (1 << (registerBit % 8))) // if the "registerBit"th bit in the mask is on
			{
				if (setTo & ((size_t)1 << valueBit))// if the "registerBit"th bit in "setTo" is on
					ownerBitField[registerBit / 8] |= (1 << (registerBit % 8));// set the "registerBit"th bit in the register since its corresponding "setTo" bit was set

				valueBit++;
			}
		}

		return *this;
	}

	Register::RegisterField::operator size_t()
	{
		size_t returnValue = 0;

		for (size_t registerBit = 0, valueBit = 0; registerBit < (bitMask.size() * 8) && valueBit < sizeof(size_t) * 8; registerBit++)// for every bit in the register
		{
			if (bitMask[registerBit / 8] & (1 << (registerBit % 8)))// if the "registerBit"th bit in the mask is on
			{
				if (ownerBitField[registerBit / 8] & (1 << (registerBit % 8)))// if the "registerBit"th bit in "setTo" is on
				{
					returnValue |= ((size_t)1 << valueBit);
				}
				valueBit++;
			}
		}

		return returnValue;
	}
}